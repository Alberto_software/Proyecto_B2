package com.alberto_programador.miprimeraaplicacion;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Creacion_de_contactos extends AppCompatActivity {
    TextView Visual, Visual2;
    EditText Entrada, Entrada2;
    Button Click1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creacion_de_contactos);
        Visual = (TextView) findViewById(R.id.pid_usu);
        Entrada = (EditText) findViewById(R.id.get_usu);
        Visual2 = (TextView) findViewById(R.id.pid_corr);
        Entrada2 = (EditText) findViewById(R.id.get_corr);
        Click1 = (Button) findViewById(R.id.button1);

        Click1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dato= Entrada.getText().toString();
                String dato2= Entrada2.getText().toString();
                Intent butVolver = new Intent(Creacion_de_contactos.this, ActivityContactos.class);
                butVolver.putExtra("usua",dato);
                butVolver.putExtra("corr",dato2);
                startActivity(butVolver);
            }
        });

    }
}
