package com.alberto_programador.miprimeraaplicacion;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private String strnombre,strfecha,strcosto,strdescripcion;
    private Button bnproyecto,bncontactos;
    private Intent intent1,intent3;
    public TextView vista1,vista2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        vista1= (TextView) findViewById(R.id.txt_nom);
        vista2= (TextView) findViewById(R.id.txt_cost);

        bnproyecto = (Button) findViewById(R.id.nproy);
        bnproyecto.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                intent1 = new Intent(MainActivity.this, NuevoProyecto.class);
                startActivity(intent1);
            }
        });

        Intent intent2 =this.getIntent();
        strnombre = intent2.getStringExtra("nombre");
        strfecha = intent2.getStringExtra("fecha");
        strcosto = intent2.getStringExtra("costo");
        strdescripcion = intent2.getStringExtra("descripcion");
        vista1.setText(strnombre);
        vista2.setText(strcosto);

        bncontactos=(Button) findViewById(R.id.ncont);
        bncontactos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent3=new Intent(MainActivity.this,ActivityContactos.class);
                startActivity(intent3);
            }
        });
    }
}