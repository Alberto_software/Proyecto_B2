package com.alberto_programador.miprimeraaplicacion;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
public class NuevoProyecto extends AppCompatActivity {

    private EditText mnombre, mfecha, mcosto,mdescripcion;
    private Intent intent2;
    private Button baceptar,bcancelar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_proyecto);

        mnombre = (EditText) findViewById(R.id.nom_proyecto);
        mfecha = (EditText) findViewById(R.id.fecha_inicio);
        mcosto = (EditText) findViewById(R.id.costo_tot);

        baceptar=(Button) findViewById(R.id.aceptar);
        baceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent2 = new Intent(NuevoProyecto.this, MainActivity.class);
                intent2.putExtra("nombre", mnombre.getText().toString());
                intent2.putExtra("fecha", mfecha.getText().toString());
                intent2.putExtra("costo", mcosto.getText().toString());
                intent2.putExtra("descripcion",mdescripcion.getText().toString());
                startActivity(intent2);
            }
        });
        bcancelar= (Button) findViewById(R.id.cancelar);
        bcancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent2= new Intent(NuevoProyecto.this,MainActivity.class);
                startActivity(intent2);
            }
        });
    }
}