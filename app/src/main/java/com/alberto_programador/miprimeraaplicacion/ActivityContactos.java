package com.alberto_programador.miprimeraaplicacion;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class ActivityContactos extends AppCompatActivity {
    ImageButton butAgre;
    String strcontactos,stre_mail;
    TextView contactos;
    TextView e_mail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactos);

        butAgre = (ImageButton) findViewById(R.id.imageButton);
        contactos= (TextView) findViewById(R.id.contact);
        e_mail= (TextView) findViewById(R.id.email);

        butAgre.setOnClickListener( new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent pasVent= new Intent(ActivityContactos.this,Creacion_de_contactos.class);
                startActivity(pasVent);
            }
        });
        Intent butVolver=this.getIntent();
        strcontactos=butVolver.getStringExtra("usua");
        stre_mail=butVolver.getStringExtra("corr");
        if(butVolver!=null) {
            contactos.setText(strcontactos);
            e_mail.setText(strcontactos);
        }
    }
}